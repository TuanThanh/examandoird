package com.example.practicaltestandroidapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.practicaltestandroidapplication.data.DbHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText edName;
    private EditText edQuantity;
    private Button btAdd;
    private Button btView;
    private DbHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        db = new DbHelper(this);
        db.getReadableDatabase();
    }

    private void initView(){
        edName = (EditText) findViewById(R.id.edName);
        edQuantity = (EditText) findViewById(R.id.edQuantity);
        btAdd = (Button) findViewById(R.id.btAdd);
        btView = (Button) findViewById(R.id.btView);
        btAdd.setOnClickListener(this);

    }

    @Override
    public void onClick(View v){
        if(v == btAdd){
            onRegister();
        }
        else if(v == btView){
            Intent intent = new Intent(MainActivity.this, ListProduct.class );
            startActivity(intent);
        }
    }

    private void onRegister(){
        if(edName.getText().toString().isEmpty()){
            Toast.makeText(this, "Please enter username", Toast.LENGTH_LONG).show();
            return;
        }

        String isAdd = db.addProduct(edName.getText().toString(), Integer.parseInt(String.valueOf(edQuantity.getText())));
        Log.d("Test register", "test");
        Toast.makeText(this, isAdd, Toast.LENGTH_LONG).show();


        Intent intent = new Intent(MainActivity.this, ListProduct.class );
        startActivity(intent);
    }

}
