package com.example.practicaltestandroidapplication.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "Customer";
    public static final int DB_VERSION = 1;

    public static String TABLE_NAME = "TBL_Customer";
    public static String ID = "_id";
    public static String NAME = "name";
    public static String QUANTITY = "quantity";

    public DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //CREATE TABLE TBL_USER(
        // id, Integer primary key,
        // name TEXT,
        // quantity TEXT

        String sql = "CREATE TABLE " + TABLE_NAME + " ( " +
                ID + " INTEGER PRIMARY KEY, " +
                NAME + " TEXT, " +
                QUANTITY + " TEXT)";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS " + TABLE_NAME;
        db.execSQL(sql);
        onCreate(db);
    }

    public String addProduct(String user, int quantity){
        Log.d("Test Add: " ,"Testttttt");
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(NAME, user);
        contentValues.put(QUANTITY, quantity);
        long idAdd = db.insert(TABLE_NAME,null, contentValues);

        if(idAdd == -1){
            return "Add fail";
        }
        db.close();
        return "Add success";
    }

    public Cursor getAllProduct(){
        SQLiteDatabase db = this.getReadableDatabase();
        String sql = "SELECT * FROM " + TABLE_NAME;
        Cursor c = db.rawQuery(sql, null);
        return c;
    }
}
